typedef struct calendar 
{
    int[] aStartHour1;     // Start hour 00-23 for morning time slice
    int[] aStartMin1;      // Start minute 00-59 for morning time slice
    int[] aStartHour2;     // Start hour 00-23 for evening time slice
    int[] aStartMin2;      // Start minute 00-59 for evening time slice
    int[] aEndHour1;       // End hour 00-23 for morning time slice
    int[] aEndMin1;        // End minute 00-59 for morning time slice
    int[] aEndHour2;       // End hour 00-23 for evening time slice
    int[] aEndMin2;        // End minute 00-59 for evening time slice
} calendar;