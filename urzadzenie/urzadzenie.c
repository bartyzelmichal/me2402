#include "urzadzenie.h"

static _integer* czesci_ogolem;
static _integer* czesci_stalych;
static _integer* czesci_ruchomych;

_integer* getCzesciOgolem()
{
	if (NULL != czesci_ogolem) {
			return czesci_ogolem;
		} else if (NULL != czesci_ruchomych || NULL != czesci_stalych) {
			_integer* ret;
			ret->value = czesci_ruchomych->value;
			if (NULL != czesci_stalych) {
				if (NULL != czesci_ruchomych ) {
					ret->value += czesci_stalych->value;
				} else {
					ret->value = czesci_stalych->value;
				}
				return ret;
			}
			return ret;
		}
		
		return NULL;
}

_integer* get_czesci_stalych()
{
	return czesci_ruchomych;
}

_integer* get_czesci_ruchomych()
{
	return czesci_ruchomych;
}

void set_czesci_stalych(_integer* value)
{
	czesci_stalych = value;
}

void set_czesci_ruchomych(_integer* value)
{
	czesci_ruchomych = value;
}
void set_czesci_ogolem(_integer* value)
{
	czesci_ogolem = value;
}