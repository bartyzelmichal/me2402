#ifndef urzadzenie_h
#define urzadzenie_h

#define NULL 0

typedef struct
{
	int value;
} _integer;

_integer* get_czesci_ogolem();
_integer* get_czesci_stalych();
_integer* get_czesci_ruchomych();

void set_czesci_stalych(_integer*);
void set_czesci_ruchomych(_integer*);
void set_czesci_ogolem(_integer*);

#endif