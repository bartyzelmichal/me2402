#include "urzadzenie.h"
#include "stdio.h"
#include "minunit.h"

int tests_run = 0;

static char* test() {
	
	mu_assert("Sth wrong", 0==0);
	return 0;
}

static char * run_test_suit() {
     mu_run_test(test);
     return 0;
}

int main(int argc, char **argv) {
	char *result = run_test_suit();

	if (result != 0) {
		printf("%s\n", result);
	}
	else {
		printf("ALL TESTS PASSED\n");
	}
	printf("Tests run: %d\n", tests_run);

	return result != 0;
}