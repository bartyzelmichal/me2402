/*
 * calc-test.c
 *
 *  Created on: Oct 8, 2019
 *      Author: michal
 */


#include <stdio.h>
#include "minunit.h"
#include "../calc/calc.h"

 int tests_run = 0;

 static char* should_add_two_positives() {
	 //arrage
     int a = 2;
	 int b = 3;

     //act
     int sum = add(a, b);

     //assert
	 mu_assert("Failed expected 5", sum == 5);

	 return 0;
 }

 static char * run_test_suit() {
     mu_run_test(should_add_two_positives);
     return 0;
 }

 int main(int argc, char **argv) {
     char *result = run_test_suit();

     if (result != 0) {
         printf("%s\n", result);
     }
     else {
         printf("ALL TESTS PASSED\n");
     }
     printf("Tests run: %d\n", tests_run);

     return result != 0;
 }
