#!/bin/bash  

TARGET=calc-test.bin
SRCS="calc/calc.c test/calc-test.c"

echo "Building tests"
gcc -o $TARGET calc/calc.c test/calc-test.c 

echo "Running tests" 
./$TARGET